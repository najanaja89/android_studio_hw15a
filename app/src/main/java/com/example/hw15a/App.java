package com.example.hw15a;

import android.app.Application;

import androidx.room.Room;

public class App extends Application {

    private AppDatabase database;
    public static  App instance;

    @Override
    public void onCreate() {
        super.onCreate();
        instance=this;
        database = Room.databaseBuilder(this, AppDatabase.class, "Office").build();
    }

    public AppDatabase getDatabase(){
        return database;
    }
}
