package com.example.hw15a;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface EmployeeDao {
    @Query("select * from employees")
    List<Employee> getAll();

    @Query("select * from employees where id = :id")
    Employee getById(long id);

    @Insert
    void insert (Employee employee);

    @Update
    void update (Employee employee);

    @Delete
    void delete (Employee employee);
}
